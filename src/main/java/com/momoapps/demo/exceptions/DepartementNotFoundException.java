package com.momoapps.demo.exceptions;

public class DepartementNotFoundException extends RuntimeException {
    public DepartementNotFoundException(Long id){
        super("Could not find departement " + id);
    }
}
