package com.momoapps.demo;

import com.momoapps.demo.model.Department;
import com.momoapps.demo.model.Employee;
import com.momoapps.demo.service.DepartmentService;
import com.momoapps.demo.service.EmployeeService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Date;

@SpringBootApplication
public class WebApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebApiApplication.class, args);
    }

    @Bean
    CommandLineRunner start(EmployeeService employeeService, DepartmentService departementService) {

        return args -> {
            employeeService.deleteAll();
            departementService.deleteAll();

            departementService.save(new Department(1L, "Finance"));
            departementService.save(new Department(2L, "IT"));
            departementService.save(new Department(3L, "Operations"));

            employeeService.save(new Employee(1L, "Sam", "Finance", "sam123@gmail.com", new Date()));
            employeeService.save(new Employee(2L, "Bob", "IT", "bob123@gmail.com", new Date()));
            employeeService.save(new Employee(3L, "May", "Operations", "may123@gmail.com", new Date()));
        };
    }

}
