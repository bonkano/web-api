package com.momoapps.demo.service;

import com.momoapps.demo.exceptions.DepartementNotFoundException;
import com.momoapps.demo.model.Department;
import com.momoapps.demo.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class DepartmentService {

    @Autowired
    private DepartmentRepository departementRepository;

    public Department save(Department departement) {
        return departementRepository.save(departement);
    }

    public Department update(Department newDepartement, Long id) {
        return departementRepository.findById(id)
                .map(departement -> {
                    departement.setDepartmentName(newDepartement.getDepartmentName());
                    return departementRepository.save(departement);
                }).orElseGet(() -> {
                    newDepartement.setDepartmentID(id);
                    return departementRepository.save(newDepartement);
                });
    }

    public List<Department> findAll() {
        return departementRepository.findAll();
    }

    public Department findById(Long id) {
        return departementRepository.findById(id)
                .orElseThrow(() -> new DepartementNotFoundException(id));
    }

    public Department delete(Long id) {
        Department departement = this.findById(id);
        if (departement == null)
            throw new RuntimeException("Departement doesn't exist");
        departementRepository.deleteById(id);
        return departement;
    }

    public void deleteAll() {
        departementRepository.deleteAll();
    }
}
