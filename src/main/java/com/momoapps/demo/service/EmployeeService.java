package com.momoapps.demo.service;

import com.momoapps.demo.exceptions.EmployeeNotFoundException;
import com.momoapps.demo.model.Employee;
import com.momoapps.demo.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public Employee save(Employee employee) {
        return employeeRepository.save(employee);
    }

    public Employee findById(Long id) {
        return employeeRepository.findById(id)
                .orElseThrow(() -> new EmployeeNotFoundException(id));
    }

    public Employee update(Employee newEmployee, Long id) {
        return employeeRepository.findById(id)
                .map(employee -> {
                    employee.setEmployeeName(newEmployee.getEmployeeName());
                    employee.setDepartment(newEmployee.getDepartment());
                    employee.setMailID(newEmployee.getMailID());
                    employee.setDoj(newEmployee.getDoj());
                    return employeeRepository.save(employee);
                })
                .orElseGet(() -> {
                    newEmployee.setEmployeeID(id);
                    return employeeRepository.save(newEmployee);
                });
    }

    /*public Employee update(Employee newEmployee) {
        return employeeRepository.findById(newEmployee.getEmployeeId())
                .map(employee -> {
                    employee.setEmployeeName(newEmployee.getEmployeeName());
                    employee.setDepartement(newEmployee.getDepartement());
                    employee.setMailId(newEmployee.getMailId());
                    employee.setDoj(newEmployee.getDoj());
                    return employeeRepository.save(employee);
                }).orElseGet(() -> {
                    return employeeRepository.save(newEmployee);
                });
    }*/

    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public void delete(Long id) {
        Employee employee = this.findById(id);
        if (employee == null)
            throw new EmployeeNotFoundException(id);
        employeeRepository.deleteById(id);
    }

    public void deleteAll() {
        employeeRepository.deleteAll();
    }
}
