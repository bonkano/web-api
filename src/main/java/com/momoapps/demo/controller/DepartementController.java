package com.momoapps.demo.controller;

import com.momoapps.demo.model.Department;
import com.momoapps.demo.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping({"/api/departments"})
public class DepartementController {

    @Autowired
    private DepartmentService departementService;

    @PostMapping
    public Department save(@RequestBody Department department) {
        return departementService.save(department);
    }

    @PutMapping("/{id}")
    public Department update(@RequestBody Department department, @PathVariable Long id) {
        return departementService.update(department, id);
    }

    @GetMapping
    public List<Department> findAll() {
        return departementService.findAll();
    }

    @GetMapping("/{id}")
    public Department findById(@PathVariable("id") Long id) {
        return departementService.findById(id);
    }

    @DeleteMapping("{id}")
    public Department delete(@PathVariable("id") Long id) {
        return departementService.delete(id);
    }
}
