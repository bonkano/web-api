package com.momoapps.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data @AllArgsConstructor @NoArgsConstructor @ToString
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DEPARTEMENT_ID")
    private Long departmentID;
    @Column(name = "DEPARTEMENT_NAME", unique = true)
    private String departmentName;
}
